package com.example.hello.controller;

import org.apache.axis.encoding.XMLType;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import org.apache.axis.client.Call;
import org.apache.axis.client.Service;
import org.w3c.dom.Document;

import javax.xml.namespace.QName;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.rpc.ParameterMode;
import javax.xml.rpc.ServiceException;
import java.io.ByteArrayInputStream;
import java.rmi.RemoteException;

@RestController
public class HelloController {
    @RequestMapping(value = "/hello", method = RequestMethod.GET)
    public String say(String taskId) throws ServiceException, RemoteException {
        try {
            String endpoint="http://localhost:55917/WebService/MobileService.asmx";
            Service service=new Service();
            Call call=(Call)service.createCall();
            call.setSOAPActionURI("http://localhost:55917/WebService/MoblieService.asmx/getPageInfoXml");
            call.setOperationName(new QName("http://tempuri.org/","getPageInfoXml"));
            call.setTargetEndpointAddress(endpoint);
            //nodeId, string dataId, string dataClass, string taskId
            call.addParameter("processId", XMLType.XSD_STRING, ParameterMode.IN);
            call.addParameter("nodeId", XMLType.XSD_STRING, ParameterMode.IN);
            call.addParameter("dataId", XMLType.XSD_STRING, ParameterMode.IN);
            call.addParameter("dataClass", XMLType.XSD_STRING, ParameterMode.IN);
            call.addParameter("taskId", XMLType.XSD_STRING, ParameterMode.IN);
            call.setOperationName(XMLType.XSD_STRING);
            String res=(String)call.invoke(new Object[]{"","","","","21FF1C73-F05B-44AC-953C-57ED6850DA33"});
            Document document = DocumentBuilderFactory.newInstance().newDocumentBuilder().parse(new ByteArrayInputStream(formatXml(res).getBytes("UTF-8")));
            return res;
        } catch (Exception e) {
            System.err.println(e.toString());
            return e.toString();
        }

    }

    public String formatXml(String xml) {
        String xmlResult = xml.replace("&", "&amp;");
        return xmlResult;
    }
}
